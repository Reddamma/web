package Bean;


//Bean for  RegisterServlet in BloodDonor to  DonorRegistrationDao in DAO
//Bean for  LoginServlet in BloodDonor to  DonorRegistrationDao in DAO

public class RequestBloodBean {
	
	private String patientName;
	
	private String BloodGroup;
	
        private String HospitalAddress;
	
        private String contactName;
	
        private int contactNumber;
        
	private String contactEmail;
	
	

	public String getpatientName() 
	{
		return patientName;
	}

	public void setpatientName(String patientName) 
	{
		this.patientName = patientName;
	}

	public String getBloodGroup() 
	{
		return BloodGroup;
	}

	public void setBloodGroup(String BloodGroup) 
	{
		this.BloodGroup = BloodGroup;
	}
       public String getHospitalAddress() 
	{
		return HospitalAddress;
	}

	public void setHospitalAddress(String HospitalAddress) 
	{
		this.HospitalAddress = HospitalAddress;
	}
        
        
        
        public String getcontactName() 
	{
		return contactName;
	}

	public void setcontactName(String contactName) 
	{
		this.contactName = contactName;
	}
	
        public int getcontactNumber() 
	{
		return contactNumber;
	}

	public void setcontactNumber(int contactNumber) 
	{
		this.contactNumber = contactNumber;
	}
	
	public String getcontactEmail() 
	{
		return contactEmail;
	}
        
	public void setcontactEmail(String contactEmail) 
	{
		this.contactEmail = contactEmail;
	}
	
}