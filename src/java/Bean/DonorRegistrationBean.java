package Bean;


//Bean for  RegisterServlet in BloodDonor to  DonorRegistrationDao in DAO
//Bean for  LoginServlet in BloodDonor to  DonorRegistrationDao in DAO

public class DonorRegistrationBean {
	
	private String userName;
	
	private String password;
	
	private int MobileNumber;
	
	private String Gender;
	
	private int Age;
	
	private String Email;
	
	private String Address;
	
	private String Bloodgroup;
	
	

	public String getuserName() 
	{
		return userName;
	}

	public void setUserName(String userName) 
	{
		this.userName = userName;
	}

	public String getPassword() 
	{
		return password;
	}

	public void setPassword(String password) 
	{
		this.password = password;
	}

	public int getMobileNumber() 
	{
		return MobileNumber;
	}

	public void setMobileNumber(int MobileNumber) 
	{
		this.MobileNumber = MobileNumber;
	}
	public String getGender() 
	{
		return Gender;
	}
	public void setGender(String Gender) 
	{
		this.Gender = Gender;
	}
	public void setAge(int Age) 
	{
		this.Age = Age;
	}
	public int getAge() 
	{
		return Age;
	}
	public String getEmail() 
	{
		return Email;
	}

	public void setEmail(String Email) 
	{
		this.Email = Email;
	}
	public String getAddress() 
	{
		return Address;
	}

	public void setAddress(String Address) 
	{
		this.Address = Address;
	}
	public String getBloodgroup() 
	{
		return Bloodgroup;
	}

	public void setBloodgroup(String Bloodgroup) 
	{
		this.Bloodgroup = Bloodgroup;
	}
}