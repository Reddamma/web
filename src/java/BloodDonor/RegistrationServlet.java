package BloodDonor;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import Bean.DonorRegistrationBean;

import Dao.DonorRegistrationDao;

@WebServlet(urlPatterns="/reg" , name="RegistrationServlet")
public class RegistrationServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
   
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		
		String userName = request.getParameter("userName");
		System.out.println("user name " + request.getParameter("userName"));
		
                String Password = request.getParameter("password");
		System.out.println("password " + request.getParameter("password"));
		
                String Mobile = request.getParameter("mobilenumber");
		System.out.println("mobile " + Mobile);
                int MobileNumber=Integer.parseInt(Mobile);
	    System.out.println("convert mobile " + MobileNumber);
               
            String Gender =request.getParameter("gender");
		System.out.println("gender" + Gender);
	    //String  a = request.getParameter("Age");
		int Age = Integer.parseInt(request.getParameter("age"));
		System.out.println("age  " + Age);
                
                String Email = request.getParameter("email");
		
		String Address = request.getParameter("address");
		
		String Bloodgroup =request.getParameter("BloodGroup");
System.out.println("slected blood  group " + Bloodgroup);
		DonorRegistrationBean DonorRegistrationBean = new DonorRegistrationBean(); //setting values to DonorRegisterationBean

		//DonorRegistrationBean.setDonorid(Donorid);
		
		DonorRegistrationBean.setUserName(userName);
		
		DonorRegistrationBean.setPassword(Password);
		
		DonorRegistrationBean.setMobileNumber(MobileNumber);
		
		DonorRegistrationBean.setGender(Gender);
		
		DonorRegistrationBean.setAge(Age);
		
		DonorRegistrationBean.setEmail(Email);
		 
		DonorRegistrationBean.setAddress(Address);
                DonorRegistrationBean.setBloodgroup(Bloodgroup);
		DonorRegistrationDao DonorRegistrationDao = new DonorRegistrationDao(); //getting result from DonorRegistrationDao

		int userValidate = DonorRegistrationDao.Register(DonorRegistrationBean);

		if (userValidate > 0) 
		{

			request.setAttribute("subscribedd", true);

			request.getRequestDispatcher("/WelcomeLogin.html").forward(request, response);

		} 
		else 
		{

			request.setAttribute("subscribedd", false);

			request.getRequestDispatcher("/DonorRegistrationPage.jsp").forward(request, response);
		}
	}

}
