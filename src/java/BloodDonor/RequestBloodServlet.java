package BloodDonor;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import Bean.RequestBloodBean;

import Dao.RequestBloodDao;

@WebServlet(urlPatterns="/Welcome" , name="RequestBloodServlet")
public class RequestBloodServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
   
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		
		String patientName = request.getParameter("patientName");
		System.out.println("patient name " + request.getParameter("patientName"));
		
                String Bloodgroup =request.getParameter("BloodGroup");
                System.out.println("Blood Group " + request.getParameter("BloodGroup"));
                
                String HospitalAddress = request.getParameter("hospitaladdress");
		System.out.println("Hospital Address " + request.getParameter("hospitaladdress"));
                
                
		String ContactName = request.getParameter("contactName");
		System.out.println("contact name " + request.getParameter("contactName"));
		
                String contact = request.getParameter("contactNumber");
		System.out.println("contact " + contact);
                int contactNumber=Integer.parseInt(contact);
                System.out.println("convert contact " + contactNumber);
               
                String ContactEmail = request.getParameter("contactEmail");
                System.out.println("contactEmail" + request.getParameter("contactEmail"));
                
                RequestBloodBean RequestBloodBean = new RequestBloodBean(); //setting values to RequestBloodBean

		//DonorRegistrationBean.setDonorid(Donorid);
		
		RequestBloodBean.setpatientName(patientName);
		
                RequestBloodBean.setBloodGroup(Bloodgroup);
		
                RequestBloodBean.setHospitalAddress(HospitalAddress);
                
                RequestBloodBean.setcontactName(ContactName);
                
                RequestBloodBean.setcontactNumber(contactNumber);
                
                RequestBloodBean.setcontactEmail(ContactEmail);
                
                
                RequestBloodDao RequestBloodDao = new RequestBloodDao(); //getting result from RequestBloodDao

		int userValidate = RequestBloodDao.Register(RequestBloodBean);

		if (userValidate > 0) 
		{

			request.setAttribute("subscribedd", true);

			request.getRequestDispatcher("/WelcomeLogin.html").forward(request, response);

		} 
		else 
		{

			request.setAttribute("subscribedd", false);

			request.getRequestDispatcher("/RequestBlood.jsp").forward(request, response);
		}
	}

}
