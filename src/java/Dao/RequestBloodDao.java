package Dao;

	import java.sql.Connection;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.sql.Statement;
        import Bean.RequestBloodBean;
	import Connect.DBConnection;
	public class RequestBloodDao 
	{
		public int Register(RequestBloodBean RequestBloodBean) // To insert data into Database for signup
		{
			int status;
		try {
				Connection con = DBConnection.conn();
				System.out.println("1111");
				PreparedStatement ps = con.prepareStatement("insert into Blood"
                                        + "( Pateint_ID,patient_Name,Blood_Group,Hospital_Address,contact_Name,contact_Number,contact_Email) "
                                        + "values(Bloodseq.nextval,?,?,?,?,?,?)");
				//ps.setString(1, RequestBloodBean.getBloodid());

				ps.setString(1, RequestBloodBean.getpatientName());
				
				ps.setString(2, RequestBloodBean.getBloodGroup());
				System.out.println("1111");
				
				ps.setString(3, RequestBloodBean.getHospitalAddress());
				System.out.println("1111");

				ps.setString(4, RequestBloodBean.getcontactName());
				System.out.println("1111");

				ps.setInt(5, RequestBloodBean.getcontactNumber());
				System.out.println("1111");

				ps.setString(6, RequestBloodBean.getcontactEmail());
				System.out.println("1111");

				
				
				status = ps.executeUpdate();
				
				if (status > 0) 
				{
					return status;
				}
			}
			
			
		catch (SQLException e) 
			{
				e.printStackTrace();
			}

			return 0;
		}
		
		public String authenticateUser(RequestBloodBean RequestBloodBean) 
																	// To authenticate user for log in
		{

			String patientName = RequestBloodBean.getpatientName();

			

			Connection con;

			Statement statement;

			ResultSet resultSet;

			String patientNameDB;

			try {

				con = DBConnection.conn();

				statement = con.createStatement();

				resultSet = statement.executeQuery("select patientName from Blood");

				while (resultSet.next()) 
				{

					patientNameDB = resultSet.getString("patientName");

					
					if (patientName.equals(patientNameDB)) 
					
					{
						return "SUCCESS";
					}
				}

			}
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			
			return "Invalid user credentials";
		}
}
