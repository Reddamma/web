package Dao;

	import java.sql.Connection;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.sql.Statement;
        import Bean.DonorRegistrationBean;
	import Connect.DBConnection;
	public class DonorRegistrationDao 
	{
		public int Register(DonorRegistrationBean DonorRegistrationBean) // To insert data into Database for signup
		{
			int status;
		try {
				Connection con = DBConnection.conn();
				System.out.println("1111");
				PreparedStatement ps = con.prepareStatement("insert into Donor"
                                        + ""
                                        + "(donorid,userName,Password,MobileNumber,Gender,Age,Email,Address,Bloodgroup) "
                                        + "values(Donorseq.nextval,?,?,?,?,?,?,?,?)");
				//ps.setString(1, DonorRegistrationBean.getDonorid());

				ps.setString(1, DonorRegistrationBean.getuserName());
				ps.setString(2, DonorRegistrationBean.getPassword());
				ps.setInt(3, DonorRegistrationBean.getMobileNumber());
				System.out.println("1111");
				ps.setString(4, DonorRegistrationBean.getGender());
				System.out.println("1111");
				ps.setInt(5, DonorRegistrationBean.getAge());
                                System.out.println("1111");
				ps.setString(6, DonorRegistrationBean.getEmail());
				System.out.println("1111");
				ps.setString(7, DonorRegistrationBean.getAddress());
				System.out.println("1111");
				ps.setString(8, DonorRegistrationBean.getBloodgroup());
				System.out.println("1111");
				status = ps.executeUpdate();
				
				if (status > 0) 
				{
					return status;
				}
			}
			
			
		catch (SQLException e) 
			{
				e.printStackTrace();
			}

			return 0;
		}
		
		public String authenticateUser(DonorRegistrationBean DonorRegistrationBean) 
																	// To authenticate user for log in
		{

			String username = DonorRegistrationBean.getuserName();

			String password = DonorRegistrationBean.getPassword();

			Connection con;

			Statement statement;

			ResultSet resultSet;

			String userNameDB;

			String passwordDB;

			try {

				con = DBConnection.conn();

				statement = con.createStatement();

				resultSet = statement.executeQuery("select username,password from donor");

				while (resultSet.next()) 
				{

					userNameDB = resultSet.getString("username");

					passwordDB = resultSet.getString("password");
					
					if (username.equals(userNameDB) && password.equals(passwordDB)) 
					
					{
						return "SUCCESS";
					}
				}

			}
			catch (SQLException e) 
			{
				e.printStackTrace();
			}
			
			return "Invalid user credentials";
		}
}
