<%-- 
    Document   : QuickTip
    Created on : Sep 3, 2019, 12:43:47 PM
    Author     : Haritha
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <div class="column">
            <div class="control-label col-sm-6" align="top"style="width:100%" >
                <form action="Welcome" method="post">
                    <h2>Welcome</h2>
                    <p style="width:100%">Blood donation never asks to be rich or poor, any healthy person can donate blood.</p>
                    <ul>
                        <li style="width:100%">In order to maintain accurate information and facilitate easy access, we recommend  that you update your profile with any recent changes such as a change in the contact number, email id or may be with the date of your most recent blood donation.</li>
                        <li style="width:100%">Your blood donation is the best social help</li>
                        <li style="width:100%">You may also help us spread the word by referring us to your friends.</li>
                        <li style="width:100%">Opportunities knock the door sometimes, so don't let it go and donate blood!</li>
                        <li style="width:100%">If you have any feedback, comments or suggestion, please feel free to share them through our contact us section. Help share this invaluable gift with someone in need.</li>
                        <br></br>
                        </body>
                        </html>
