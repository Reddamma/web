<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Send an e-mail</title>
        <style>
            .container {
                padding: 10px;
            }
            .mySlide {
                display:none;
                margin-left:initial;
                margin-right:inherit;
                height:730px;
            }
            form {
                background: url('background4.jpg');
                background-size: 1600px 1000px;
                width: 90%;
                margin-left: 1px;
                margin-top: 1px;
                height: 700px;
                margin: 10px auto;
            }

            form div {
                margin: 1px auto;
            }
        </style>
    </head>
    <body style="background-color:  #b3ffb3">
        <div class="container">
            <div style="text-align: center">
                <h2>Sending Email Page</h2>
                <br>
            </div>
            <div style="text-align: center">
                <p>THE BLOOD YOU DONATE GIVES SOMEONE ANOTHER CHANCE AT LIFE. ONE
                    DAY THAT SOMEONE MAY BE A CLOSE RELATIVE, A FRIEND,
                    A LOVED ONE OR
                    EVEN YOU</p>
            </div><br></br>

            <div class="column">
                <div class="control-label col-sm-7" align="center">
                    <form action="EmailSendingServlet" method="post">
                        <table border="0" width="35%" align="center">

                            <tr>
                                <td width="50%"> Enter Sender Email</td>
                                <td><input type="text" name="email" size="50"/></td>
                            </tr>
                            <tr>
                                <td>Subject </td>
                                <td><input type="text" name="subject" size="50"/></td>
                            </tr>
                            <tr>
                                <td>Content </td>
                                <td><textarea rows="10" cols="39" name="content"></textarea> </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center"><input type="submit" value="Send"/></td>
                            </tr>

                            <tr>
                                <td colspan="2" align="center"><input type="submit" value="Reset"/></td>
                            </tr>
                        </table>
                </div>
            </div>
        </form>
    </div>
</body>
</html>