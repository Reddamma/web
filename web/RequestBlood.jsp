<%-- 
    Document   : RequestBlood
    Created on : Sep 4, 2019, 1:52:04 PM
    Author     : Haritha
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Form validation with JavaScript</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <style>
            .container {
                padding: 10px;
            }

            #phoneId {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }
            #emailId {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }


            #nameId {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            #emailId {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            #patientId {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }


            #addressId {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            .submitbtn {
                background-color: #4CAF50;
                color: white;
                padding: 12px 20px;
                margin: 5px 0;
                border: none;
                cursor: pointer;
                width: 100%;
                opacity: 0.9;
            }
            .mySlides {
                display:none;
                margin-left:initial;
                margin-right:inherit;
                height:730px;
            }
            form {
                background: url('background4.jpg');
                background-size: 1600px 1000px;
                width: 90%;
                margin-left: 1px;
                margin-top: 1px;
                height: 700px;
                margin: 10px auto;
            }

            form div {
                margin: 1px auto;
            }
            .navbar {
                overflow: hidden;
                height:40px;
                background-color: #333;
            }
            .navbar a {
                float: left;
                font-size: 25px;
                color: white;
                text-align: center; 
                padding: 14px 16px;
                text-decoration: none;
            }
            .navbar a:hover, .dropdown:hover .dropbtn {
                background-color: green;
            }
        </style>
    </head>
    <body style="background-color: #cc0066">
        <div class="container">
            <div class="navbar" style="background-color:  #1a0033">
                 <a href="index.html">HOME</a>
		 <a href="SearchDonors.jsp">SEARCH DONORS</a>
                 <a href="AboutUs.jsp">ABOUT US</a>
		 <a href="BloodTips.jsp">BLOOD TIPS</a>
                 <a href="RequestBlood.jsp">REQUEST BLOOD</a>
		 <a href="ContactUs.jsp">CONTACT US</a>   
            </div>       
            </div>

            <div class="row">
                <div class="control-label col-sm-7" align="left" >
                    <img class="mySlides" src="DonateBloodImage.jpg" style="width: 100%">
                    <img class="mySlides" src="RealityImage.jpg" style="width:100%">
                    <img class="mySlides" src="BloodTypes.jpg" style="width:100%">
                    <img class="mySlides" src="DonationImage.jpg" style="width:100%">
                    <img class="mySlides" src="saveLifeImage.jpg" style="width:100%"> 
                </div>
                <div class="column">
                    <div class="control-label col-sm-5" align="top" >

                        <form action="Welcome" method="POST">
                            <h1>
                                <center>Request Details</center>
                            </h1>
                            <h2>Patient Details:</h2>
                            <div>
                                <label>Patient Name</label> <input type="text" id="nameId"
                                                                   placeholder="Name" required name="patientName" />
                            </div>
                            <label for="BloodGroup">Blood Group</label>

                            <select id="BloodGroup" name="BloodGroup">
                                <option value="A+">A+</option>
                                <option value="B+">B+</option>
                                <option value="O+">O+</option>
                                <option value="O-">O-</option>
                                <option value="A-">A-</option>
                                <option value="B-">B-</option>
                                <option value="AB+">AB+</option>
                                <option value="AB-">AB-</option>
                            </select>

                            <div>
                                <label>Hospital Address</label> 
                                <input type="address" id="addressId"
                                       placeholder=" address" name="hospitaladdress" required />
                            </div>

                            <h2>Contact Details:</h2>

                            <div>
                                <label>Contact Name</label> <input type="text" id="nameId"
                                                                   placeholder="Name" required name="contactName" />
                            </div>
                            <div>
                                <label>Contact Number</label>
                                <input type="tel" id="phoneId"
                                       placeholder=" contactnumber" name="contactNumber" required />
                            </div>
                            <div>
                                <label>Contact Email</label> <input type="email" id="emailId"
                                                                    placeholder=" Email" name="contactEmail" required />
                            </div>


                            <div>
                                <input type="submit" class="submitbtn"/>
                            </div><br>

                        </form>
                    </div>

                    </body>

                    <script>
                        var myIndex = 0;
                        carousel();
                        function carousel() {
                            var i;
                            var x = document.getElementsByClassName("mySlides");
                            for (i = 0; i < x.length; i++) {
                                x[i].style.display = "none";
                            }
                            myIndex++;
                            if (myIndex > x.length) {
                                myIndex = 1
                            }
                            x[myIndex - 1].style.display = "block";
                            setTimeout(carousel, 1000); // Change image every 2 seconds
                        }
                    </script>
                    </html>