<%-- 
    Document   : AboutUs
    Created on : Sep 4, 2019, 9:37:09 PM
    Author     : Haritha
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
 <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<style>
body {
  font-family: Arial, Helvetica, sans-serif;

}
* {
  box-sizing: border-box;
}

/* Style inputs */
input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

/* Style the container/contact section */
.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 10px;
}

/* Create two columns that float next to eachother */
.column {
  float: left;
  width: 50%;
  margin-top: 6px;
  padding: 20px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column, input[type=submit] {
    width: 100%;
    margin-top: 0;
    font-size:30px;
  }
}

.navbar {
         overflow: hidden;
         height:40px;
         background-color: #333;
         }
         .navbar a {
         float: left;
         font-size: 25px;
         color: white;
         text-align: center; 
         padding: 14px 16px;
         text-decoration: none;
         }
          .navbar a:hover, .dropdown:hover .dropbtn {
         background-color: green;
         }

</style>
</head>
<body style="background-color:  #ffad33">

<div class="container">
    <div class="navbar" style="background-color:  #1a0033">
           <a href="index.html">HOME</a>
		 <a href="SearchDonors.jsp">SEARCH DONORS</a>
                 <a href="AboutUs.jsp">ABOUT US</a>
		 <a href="BloodTips.jsp">BLOOD TIPS</a>
                 <a href="RequestBlood.jsp">REQUEST BLOOD</a>
		 <a href="ContactUs.jsp">CONTACT US</a>            
         </div>
  <div style="text-align:center">
    <h2>ABOUT US</h2>
    <p>Swing by for a cup of coffee, or leave us a message:</p><br></br>
  </div>
  <div class="row">
		<div class="control-label col-sm-6" align="top">
			<img class="mySlide" src="RegistrationsImage.jpg" style="width: 100%">
		</div>
    <div class="column">
        <div class="control-label col-sm-6" align="top"style="width:100%" ><br>
      <form action="blood tips" method="post">
          <p style="width:100%">Blood donation never asks to be rich or poor, 
              any healthy person can donate blood.</p>
          <ul><br></br>
<li style="width:100%">'Blood Bank India' is the first product resulted out of the community welfare initiative called 'People Project'
    from US is Technologies. Universally, 'Blood' is recognized as the most precious element that sustains life. 
    It saves innumerable lives across the world in a variety of conditions. <br>
    Once in every 2- seconds, someone, somewhere is desperately in need of blood.
    More than 29 million units of blood components are transfused every year. <br>
    The need for blood is great - on any given day, approximately 39,000 units of Red Blood Cells are needed. 
    Each year, we could meet only up to 1% (approx) of our nation?s demand for blood transfusion.<br>
    Despite the increase in the number of donors, blood remains in short supply during emergencies, mainly attributed
    to the lack of information and accessibility. 
    We positively believe this tool can overcome most of these challenges by 
    effectively connecting the blood donors with the blood recipients. </li>

</ul><br>

<div><a href="Login.html"></a>
</div>
</form>
   </body>

</html>
