package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class SearchDonors_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css\">\r\n");
      out.write("        <style>\r\n");
      out.write("            body {\r\n");
      out.write("                font-family: Arial, Helvetica, sans-serif;\r\n");
      out.write("                margin-left: 150px;\r\n");
      out.write("            }\r\n");
      out.write("            * {\r\n");
      out.write("                box-sizing: border-box;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            /* Style inputs */\r\n");
      out.write("            input[type=text], select, textarea {\r\n");
      out.write("                width: 100%;\r\n");
      out.write("                padding: 12px;\r\n");
      out.write("                border: 1px solid #ccc;\r\n");
      out.write("                margin-top: 6px;\r\n");
      out.write("                margin-bottom: 16px;\r\n");
      out.write("                resize: vertical;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            input[type=submit] {\r\n");
      out.write("                background-color: #4CAF50;\r\n");
      out.write("                color: white;\r\n");
      out.write("                padding: 12px 20px;\r\n");
      out.write("                border: none;\r\n");
      out.write("                cursor: pointer;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            input[type=submit]:hover {\r\n");
      out.write("                background-color: #45a049;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            /* Style the container/contact section */\r\n");
      out.write("            .container {\r\n");
      out.write("                border-radius: 5px;\r\n");
      out.write("                background-color: #f2f2f2;\r\n");
      out.write("                padding: 10px;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            /* Create two columns that float next to eachother */\r\n");
      out.write("            .column {\r\n");
      out.write("                float: left;\r\n");
      out.write("                width: 50%;\r\n");
      out.write("                margin-top: 6px;\r\n");
      out.write("                padding: 20px;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            /* Clear floats after the columns */\r\n");
      out.write("            .row:after {\r\n");
      out.write("                content: \"\";\r\n");
      out.write("                display: table;\r\n");
      out.write("                clear: both;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("            /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */\r\n");
      out.write("            @media screen and (max-width: 700px) {\r\n");
      out.write("                .column, input[type=submit] {\r\n");
      out.write("                    width: 100%;\r\n");
      out.write("                    margin-top: 0;\r\n");
      out.write("                }\r\n");
      out.write("            }\r\n");
      out.write("            .navbar {\r\n");
      out.write("                overflow: hidden;\r\n");
      out.write("                height:40px;\r\n");
      out.write("                background-color: #333;\r\n");
      out.write("            }\r\n");
      out.write("            .navbar a {\r\n");
      out.write("                float: left;\r\n");
      out.write("                font-size: 25px;\r\n");
      out.write("                color: white;\r\n");
      out.write("                text-align: center; \r\n");
      out.write("                padding: 14px 16px;\r\n");
      out.write("                text-decoration: none;\r\n");
      out.write("            }\r\n");
      out.write("            .navbar a:hover, .dropdown:hover .dropbtn {\r\n");
      out.write("                background-color: green;\r\n");
      out.write("            }\r\n");
      out.write("\r\n");
      out.write("        </style>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body style=\"background-color:#4d0026\">\r\n");
      out.write("        <div class=\"container\">\r\n");
      out.write("            <div class=\"navbar\" style=\"background-color:  #1a0033\">\r\n");
      out.write("               <a href=\"index.html\">HOME</a>\r\n");
      out.write("\t\t <a href=\"index.html\">SEARCH DONORS</a>\r\n");
      out.write("                 <a href=\"index.html\">ABOUT US</a>\r\n");
      out.write("\t\t <a href=\"index.html\">BLOOD TIPS</a>\r\n");
      out.write("                 <a href=\"index.html\">REQUEST BLOOD</a>\r\n");
      out.write("\t\t <a href=\"index.html\">CONTACT US</a>   \r\n");
      out.write("            </div>\r\n");
      out.write("            <div style=\"text-align:center\">\r\n");
      out.write("                <h2>Search Donor Details</h2>\r\n");
      out.write("                <p>You need a big heart and free mind for blood donation and not money and strength.</p>\r\n");
      out.write("            </div>\r\n");
      out.write("            <div class=\"row\">\r\n");
      out.write("                <div class=\"control-label col-sm-5\" align=\"top\" ><br>\r\n");
      out.write("                    <img class=\"mySlide\" src=\"download8.jpg\" style=\"width:250%\">\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"column\">\r\n");
      out.write("                    <div class=\"control-label col-sm-7\" align=\"top\" style=\"margin-left: 200px\" >\r\n");
      out.write("                        <form action=\"DonorView.jsp   \" method=\"post\"><br></br>\r\n");
      out.write("                            <label for=\"BloodGroup\">BloodGroup</label>\r\n");
      out.write("\r\n");
      out.write("                            <select id=\"BloodGroup\" name=\"BloodGroup\">\r\n");
      out.write("                                <option value=\"A+\">A+</option>\r\n");
      out.write("                                <option value=\"B+\">B+</option>\r\n");
      out.write("                                <option value=\"O+\">O+</option>\r\n");
      out.write("                                <option value=\"O-\">O-</option>\r\n");
      out.write("                                <option value=\"A-\">A-</option>\r\n");
      out.write("                                <option value=\"B-\">B-</option>\r\n");
      out.write("                                <option value=\"AB+\">AB+</option>\r\n");
      out.write("                                <option value=\"AB-\">AB-</option>\r\n");
      out.write("                            </select>\r\n");
      out.write("                            <label for=\"address\">address</label>\r\n");
      out.write("\r\n");
      out.write("                            <select id=\"address\" name=\"address\">\r\n");
      out.write("                                <option value=\"Tirupati\">Tirupati</option>\r\n");
      out.write("                                <option value=\"Chennai\">Chennai</option>\r\n");
      out.write("                                <option value=\"Bangalore\">Bangalore</option>\r\n");
      out.write("                                <option value=\"Nellore\">Nellore</option>\r\n");
      out.write("                                <option value=\"Chittoor\">Chittoor</option>\r\n");
      out.write("                                <option value=\"Vijayavada\">Vijayavada</option>\r\n");
      out.write("                                <option value=\"Visakapatnam\">Visakapatnam</option>\r\n");
      out.write("                                <option value=\"Hydhrabad\">Hydhrabad</option>\r\n");
      out.write("                            </select>\r\n");
      out.write("                            <input type=\"submit\" value=\"Search\"> \r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                            </div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </form>\r\n");
      out.write("                        </body>\r\n");
      out.write("\r\n");
      out.write("                        </html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
