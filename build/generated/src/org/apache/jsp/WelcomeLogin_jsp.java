package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class WelcomeLogin_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css\">\n");
      out.write("        <style>\n");
      out.write("            body {\n");
      out.write("                font-family: Arial, Helvetica, sans-serif;\n");
      out.write("\n");
      out.write("            }\n");
      out.write("            * {\n");
      out.write("                box-sizing: border-box;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            /* Style inputs */\n");
      out.write("            input[type=text], select, textarea {\n");
      out.write("                width: 100%;\n");
      out.write("                padding: 12px;\n");
      out.write("                border: 1px solid #ccc;\n");
      out.write("                margin-top: 6px;\n");
      out.write("                margin-bottom: 16px;\n");
      out.write("                resize: vertical;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            input[type=submit] {\n");
      out.write("                background-color: #4CAF50;\n");
      out.write("                color: white;\n");
      out.write("                padding: 12px 20px;\n");
      out.write("                border: none;\n");
      out.write("                cursor: pointer;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            input[type=submit]:hover {\n");
      out.write("                background-color: #45a049;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            /* Style the container/contact section */\n");
      out.write("            .container {\n");
      out.write("                border-radius: 5px;\n");
      out.write("                background-color: #f2f2f2;\n");
      out.write("                padding: 10px;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            /* Create two columns that float next to eachother */\n");
      out.write("            .column {\n");
      out.write("                float: left;\n");
      out.write("                width: 50%;\n");
      out.write("                margin-top: 6px;\n");
      out.write("                padding: 20px;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            /* Clear floats after the columns */\n");
      out.write("            .row:after {\n");
      out.write("                content: \"\";\n");
      out.write("                display: table;\n");
      out.write("                clear: both;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */\n");
      out.write("            @media screen and (max-width: 600px) {\n");
      out.write("                .column, input[type=submit] {\n");
      out.write("                    width: 100%;\n");
      out.write("                    margin-top: 0;\n");
      out.write("                    font-size:30px;\n");
      out.write("                }\n");
      out.write("            }\n");
      out.write("\n");
      out.write("        </style>\n");
      out.write("    <html>\n");
      out.write("        <head>\n");
      out.write("            <style>\n");
      out.write("                .navbar {\n");
      out.write("                    overflow: hidden;\n");
      out.write("                    height:80px;\n");
      out.write("                    background-color: #333;\n");
      out.write("                }\n");
      out.write("                .navbar a {\n");
      out.write("                    float: left;\n");
      out.write("                    font-size: 20px;\n");
      out.write("                    color: white;\n");
      out.write("                    text-align: center;\n");
      out.write("                    padding: 14px 16px;\n");
      out.write("                    text-decoration: none;\n");
      out.write("                }\n");
      out.write("                input[type=submit]:hover {\n");
      out.write("                background-color: #45a049;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            </style>\n");
      out.write("        </head>\n");
      out.write("        <body style=\"background-color:deeppink\">\n");
      out.write("\n");
      out.write("            <div class=\"container\">\n");
      out.write("                <div style=\"text-align:center\">\n");
      out.write("                    <h2>Welcome To Blood Donor System</h2>\n");
      out.write("                    <p>Swing by for a cup of coffee, or leave us a message:</p>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"control-label col-sm-6\" align=\"top\">\n");
      out.write("                        <img class=\"mySlide\" src=\"quiz.png\" style=\"width: 100%\">\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"column\">\n");
      out.write("                        <div class=\"control-label col-sm-6\" align=\"top\"style=\"width:100%\" >\n");
      out.write("                            <form action=\"Welcome\" method=\"post\">\n");
      out.write("                                <h2>Welcome</h2>\n");
      out.write("                                <p style=\"width:100%\">Blood donation never asks to be rich or poor, any healthy person can donate blood.</p>\n");
      out.write("                                <ul>\n");
      out.write("                                    <li style=\"width:100%\">In order to maintain accurate information and facilitate easy access, we recommend  that you update your profile with any recent changes such as a change in the contact number, email id or may be with the date of your most recent blood donation.</li>\n");
      out.write("                                    <li style=\"width:100%\">Your blood donation is the best social help</li>\n");
      out.write("                                    <li style=\"width:100%\">You may also help us spread the word by referring us to your friends.</li>\n");
      out.write("                                    <li style=\"width:100%\">Opportunities knock the door sometimes, so don't let it go and donate blood!</li>\n");
      out.write("                                    <li style=\"width:100%\">If you have any feedback, comments or suggestion, please feel free to share them through our contact us section. Help share this invaluable gift with someone in need.</li>\n");
      out.write("                                </ul><br></br>\n");
      out.write("                                <div class=\"navbar\" style=\"background-color:  #1a0033\">\n");
      out.write("                                    <a href=\"PatientView.jsp\">Blood Requests</a>\n");
      out.write("                                    <a href=\"QuickTip.jsp\">Quick Tip</a>\n");
      out.write("\n");
      out.write("                                    <div><a href=\"Login.html\"></a>\n");
      out.write("                                    </div>\n");
      out.write("                            </form>\n");
      out.write("                            </body>\n");
      out.write("                            </html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
