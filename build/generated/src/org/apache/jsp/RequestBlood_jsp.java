package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class RequestBlood_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\n");
      out.write("        <title>Form validation with JavaScript</title>\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css\">\n");
      out.write("        <style>\n");
      out.write("            .container {\n");
      out.write("                padding: 10px;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            #phoneId {\n");
      out.write("                width: 100%;\n");
      out.write("                padding: 12px 20px;\n");
      out.write("                margin: 8px 0;\n");
      out.write("                display: inline-block;\n");
      out.write("                border: 1px solid #ccc;\n");
      out.write("                box-sizing: border-box;\n");
      out.write("            }\n");
      out.write("            #emailId {\n");
      out.write("                width: 100%;\n");
      out.write("                padding: 12px 20px;\n");
      out.write("                margin: 8px 0;\n");
      out.write("                display: inline-block;\n");
      out.write("                border: 1px solid #ccc;\n");
      out.write("                box-sizing: border-box;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("\n");
      out.write("            #nameId {\n");
      out.write("                width: 100%;\n");
      out.write("                padding: 12px 20px;\n");
      out.write("                margin: 8px 0;\n");
      out.write("                display: inline-block;\n");
      out.write("                border: 1px solid #ccc;\n");
      out.write("                box-sizing: border-box;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            #emailId {\n");
      out.write("                width: 100%;\n");
      out.write("                padding: 12px 20px;\n");
      out.write("                margin: 8px 0;\n");
      out.write("                display: inline-block;\n");
      out.write("                border: 1px solid #ccc;\n");
      out.write("                box-sizing: border-box;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            #patientId {\n");
      out.write("                width: 100%;\n");
      out.write("                padding: 12px 20px;\n");
      out.write("                margin: 8px 0;\n");
      out.write("                display: inline-block;\n");
      out.write("                border: 1px solid #ccc;\n");
      out.write("                box-sizing: border-box;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("\n");
      out.write("            #addressId {\n");
      out.write("                width: 100%;\n");
      out.write("                padding: 12px 20px;\n");
      out.write("                margin: 8px 0;\n");
      out.write("                display: inline-block;\n");
      out.write("                border: 1px solid #ccc;\n");
      out.write("                box-sizing: border-box;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            .submitbtn {\n");
      out.write("                background-color: #4CAF50;\n");
      out.write("                color: white;\n");
      out.write("                padding: 12px 20px;\n");
      out.write("                margin: 5px 0;\n");
      out.write("                border: none;\n");
      out.write("                cursor: pointer;\n");
      out.write("                width: 100%;\n");
      out.write("                opacity: 0.9;\n");
      out.write("            }\n");
      out.write("            .mySlides {\n");
      out.write("                display:none;\n");
      out.write("                margin-left:initial;\n");
      out.write("                margin-right:inherit;\n");
      out.write("                height:730px;\n");
      out.write("            }\n");
      out.write("            form {\n");
      out.write("                background: url('background4.jpg');\n");
      out.write("                background-size: 1600px 1000px;\n");
      out.write("                width: 90%;\n");
      out.write("                margin-left: 1px;\n");
      out.write("                margin-top: 1px;\n");
      out.write("                height: 700px;\n");
      out.write("                margin: 10px auto;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            form div {\n");
      out.write("                margin: 1px auto;\n");
      out.write("            }\n");
      out.write("            .navbar {\n");
      out.write("                overflow: hidden;\n");
      out.write("                height:40px;\n");
      out.write("                background-color: #333;\n");
      out.write("            }\n");
      out.write("            .navbar a {\n");
      out.write("                float: left;\n");
      out.write("                font-size: 25px;\n");
      out.write("                color: white;\n");
      out.write("                text-align: center; \n");
      out.write("                padding: 14px 16px;\n");
      out.write("                text-decoration: none;\n");
      out.write("            }\n");
      out.write("            .navbar a:hover, .dropdown:hover .dropbtn {\n");
      out.write("                background-color: green;\n");
      out.write("            }\n");
      out.write("        </style>\n");
      out.write("    </head>\n");
      out.write("    <body style=\"background-color: #cc0066\">\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <div class=\"navbar\" style=\"background-color:  #1a0033\">\n");
      out.write("                 <a href=\"index.html\">HOME</a>\n");
      out.write("\t\t <a href=\"SearchDonors.jsp\">SEARCH DONORS</a>\n");
      out.write("                 <a href=\"AboutUs.jsp\">ABOUT US</a>\n");
      out.write("\t\t <a href=\"BloodTips.jsp\">BLOOD TIPS</a>\n");
      out.write("                 <a href=\"RequestBlood.jsp\">REQUEST BLOOD</a>\n");
      out.write("\t\t <a href=\"ContactUs.jsp\">CONTACT US</a>   \n");
      out.write("            </div>       \n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"control-label col-sm-7\" align=\"left\" >\n");
      out.write("                    <img class=\"mySlides\" src=\"download.jpg\" style=\"width: 100%\">\n");
      out.write("                    <img class=\"mySlides\" src=\"Image1.jpg\" style=\"width:100%\">\n");
      out.write("                    <img class=\"mySlides\" src=\"image2.jpg\" style=\"width:100%\">\n");
      out.write("                    <img class=\"mySlides\" src=\"image3.jpg\" style=\"width:100%\">\n");
      out.write("                    <img class=\"mySlides\" src=\"image5.jpg\" style=\"width:100%\"> \n");
      out.write("                </div>\n");
      out.write("                <div class=\"column\">\n");
      out.write("                    <div class=\"control-label col-sm-5\" align=\"top\" >\n");
      out.write("\n");
      out.write("                        <form action=\"Welcome\" method=\"POST\">\n");
      out.write("                            <h1>\n");
      out.write("                                <center>Request Details</center>\n");
      out.write("                            </h1><br>\n");
      out.write("                            <h2>Patient Details:</h2>\n");
      out.write("                            <div>\n");
      out.write("                                <label>Patient Name</label> <input type=\"text\" id=\"nameId\"\n");
      out.write("                                                                   placeholder=\"Name\" required name=\"patientName\" />\n");
      out.write("                            </div>\n");
      out.write("                            <label for=\"BloodGroup\">Blood Group</label>\n");
      out.write("\n");
      out.write("                            <select id=\"BloodGroup\" name=\"BloodGroup\">\n");
      out.write("                                <option value=\"A+\">A+</option>\n");
      out.write("                                <option value=\"B+\">B+</option>\n");
      out.write("                                <option value=\"O+\">O+</option>\n");
      out.write("                                <option value=\"O-\">O-</option>\n");
      out.write("                                <option value=\"A-\">A-</option>\n");
      out.write("                                <option value=\"B-\">B-</option>\n");
      out.write("                                <option value=\"AB+\">AB+</option>\n");
      out.write("                                <option value=\"AB-\">AB-</option>\n");
      out.write("                            </select>\n");
      out.write("\n");
      out.write("                            <div>\n");
      out.write("                                <label>Hospital Address</label> \n");
      out.write("                                <input type=\"address\" id=\"addressId\"\n");
      out.write("                                       placeholder=\" address\" name=\"hospitaladdress\" required />\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <h2>Contact Details:</h2>\n");
      out.write("\n");
      out.write("                            <div>\n");
      out.write("                                <label>Contact Name</label> <input type=\"text\" id=\"nameId\"\n");
      out.write("                                                                   placeholder=\"Name\" required name=\"contactName\" />\n");
      out.write("                            </div>\n");
      out.write("                            <div>\n");
      out.write("                                <label>Contact Number</label>\n");
      out.write("                                <input type=\"tel\" id=\"phoneId\"\n");
      out.write("                                       placeholder=\" contactnumber\" name=\"contactNumber\" required />\n");
      out.write("                            </div>\n");
      out.write("                            <div>\n");
      out.write("                                <label>Contact Email</label> <input type=\"email\" id=\"emailId\"\n");
      out.write("                                                                    placeholder=\" Email\" name=\"contactEmail\" required />\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                            <div>\n");
      out.write("                                <input type=\"submit\" class=\"submitbtn\"/>\n");
      out.write("                            </div><br>\n");
      out.write("\n");
      out.write("                        </form>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    </body>\n");
      out.write("\n");
      out.write("                    <script>\n");
      out.write("                        var myIndex = 0;\n");
      out.write("                        carousel();\n");
      out.write("                        function carousel() {\n");
      out.write("                            var i;\n");
      out.write("                            var x = document.getElementsByClassName(\"mySlides\");\n");
      out.write("                            for (i = 0; i < x.length; i++) {\n");
      out.write("                                x[i].style.display = \"none\";\n");
      out.write("                            }\n");
      out.write("                            myIndex++;\n");
      out.write("                            if (myIndex > x.length) {\n");
      out.write("                                myIndex = 1\n");
      out.write("                            }\n");
      out.write("                            x[myIndex - 1].style.display = \"block\";\n");
      out.write("                            setTimeout(carousel, 1000); // Change image every 2 seconds\n");
      out.write("                        }\n");
      out.write("                    </script>\n");
      out.write("                    </html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
