package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class SearchPatients_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("  \n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write(" <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css\">\n");
      out.write("<style>\n");
      out.write("body {\n");
      out.write("  font-family: Arial, Helvetica, sans-serif;\n");
      out.write("    margin-left: 150px;\n");
      out.write("}\n");
      out.write("* {\n");
      out.write("  box-sizing: border-box;\n");
      out.write("}\n");
      out.write("\n");
      out.write("/* Style inputs */\n");
      out.write("input[type=text], select, textarea {\n");
      out.write("  width: 100%;\n");
      out.write("  padding: 12px;\n");
      out.write("  border: 1px solid #ccc;\n");
      out.write("  margin-top: 6px;\n");
      out.write("  margin-bottom: 16px;\n");
      out.write("  resize: vertical;\n");
      out.write("}\n");
      out.write("\n");
      out.write("input[type=submit] {\n");
      out.write("  background-color: #4CAF50;\n");
      out.write("  color: white;\n");
      out.write("  padding: 12px 20px;\n");
      out.write("  border: none;\n");
      out.write("  cursor: pointer;\n");
      out.write("}\n");
      out.write("\n");
      out.write("input[type=submit]:hover {\n");
      out.write("  background-color: #45a049;\n");
      out.write("}\n");
      out.write("\n");
      out.write("/* Style the container/contact section */\n");
      out.write(".container {\n");
      out.write("  border-radius: 5px;\n");
      out.write("  background-color: #f2f2f2;\n");
      out.write("  padding: 10px;\n");
      out.write("}\n");
      out.write("\n");
      out.write("/* Create two columns that float next to eachother */\n");
      out.write(".column {\n");
      out.write("  float: left;\n");
      out.write("  width: 50%;\n");
      out.write("  margin-top: 6px;\n");
      out.write("  padding: 20px;\n");
      out.write("}\n");
      out.write("\n");
      out.write("/* Clear floats after the columns */\n");
      out.write(".row:after {\n");
      out.write("  content: \"\";\n");
      out.write("  display: table;\n");
      out.write("  clear: both;\n");
      out.write("}\n");
      out.write("\n");
      out.write("/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */\n");
      out.write("@media screen and (max-width: 600px) {\n");
      out.write("  .column, input[type=submit] {\n");
      out.write("    width: 100%;\n");
      out.write("    margin-top: 0;\n");
      out.write("  }\n");
      out.write("}\n");
      out.write(".navbar {\n");
      out.write("         overflow: hidden;\n");
      out.write("         height:40px;\n");
      out.write("         background-color: #333;\n");
      out.write("         }\n");
      out.write("         .navbar a {\n");
      out.write("         float: left;\n");
      out.write("         font-size: 25px;\n");
      out.write("         color: white;\n");
      out.write("         text-align: center; \n");
      out.write("         padding: 14px 16px;\n");
      out.write("         text-decoration: none;\n");
      out.write("         }\n");
      out.write("          .navbar a:hover, .dropdown:hover .dropbtn {\n");
      out.write("         background-color: green;\n");
      out.write("         }\n");
      out.write("\n");
      out.write("</style>\n");
      out.write("</head>\n");
      out.write("<body style=\"background-color:#4d0026\">\n");
      out.write("<div class=\"container\">\n");
      out.write("    <div class=\"navbar\" style=\"background-color:  #1a0033\">\n");
      out.write("           <a href=\"index.html\">Back</a>        \n");
      out.write("         </div>\n");
      out.write("  <div style=\"text-align:center\">\n");
      out.write("    <h2>Search Patient Details</h2>\n");
      out.write("    <p>You need a big heart and free mind for blood donation and not money and strength.</p>\n");
      out.write("  </div>\n");
      out.write("  <div class=\"row\">\n");
      out.write("      <div class=\"control-label col-sm-5\" align=\"top\" ><br>\n");
      out.write("         <img class=\"mySlide\" src=\"download9.jpg\" style=\"width:250%\">\n");
      out.write("    </div>\n");
      out.write("    <div class=\"column\">\n");
      out.write("    <div class=\"control-label col-sm-7\" align=\"top\" style=\"margin-left: 200px\" >\n");
      out.write("        <form action=\"PatientView.jsp   \" method=\"post\"><br></br>\n");
      out.write("      <label for=\"BloodGroup\">BloodGroup</label>\n");
      out.write("      \n");
      out.write("        <select id=\"BloodGroup\" name=\"BloodGroup\">\n");
      out.write("\t\t\t\t\t\t\t<option value=\"A+\">A+</option>\n");
      out.write("\t\t\t\t\t\t\t<option value=\"B+\">B+</option>\n");
      out.write("\t\t\t\t\t\t\t<option value=\"O+\">O+</option>\n");
      out.write("\t\t\t\t\t\t\t<option value=\"O-\">O-</option>\n");
      out.write("\t\t\t\t\t\t\t<option value=\"A-\">A-</option>\n");
      out.write("\t\t\t\t\t\t\t<option value=\"B-\">B-</option>\n");
      out.write("\t\t\t\t\t\t\t<option value=\"AB+\">AB+</option>\n");
      out.write("\t\t\t\t\t\t\t<option value=\"AB-\">AB-</option>\n");
      out.write("\t\t\t\t\t\t\t</select>\n");
      out.write("        <label for=\"address\">Hospital Address</label>\n");
      out.write("        \n");
      out.write("        <select id=\"hospitaladdress\" name=\"hospitaladdress\">\n");
      out.write("                            <option value=\"Tirupati\">Tirupati</option>\n");
      out.write("\t\t\t\t\t\t\t<option value=\"Chennai\">Chennai</option>\n");
      out.write("\t\t\t\t\t\t\t<option value=\"Bangalore\">Bangalore</option>\n");
      out.write("\t\t\t\t\t\t\t<option value=\"Nellore\">Nellore</option>\n");
      out.write("\t\t\t\t\t\t\t<option value=\"Chittoor\">Chittoor</option>\n");
      out.write("\t\t\t\t\t\t\t<option value=\"Vijayavada\">Vijayavada</option>\n");
      out.write("\t\t\t\t\t\t\t<option value=\"Visakapatnam\">Visakapatnam</option>\n");
      out.write("\t\t\t\t\t\t\t<option value=\"Hydhrabad\">Hydhrabad</option>\n");
      out.write("        </select>\n");
      out.write("        <input type=\"submit\" value=\"Search\"> \n");
      out.write("        \n");
      out.write("             \n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("       </form>\n");
      out.write("</body>\n");
      out.write("  \n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
