<%-- 
   Document   : update
   Created on : sep 6, 2019, 9:10:13 PM
   Author     : Haritha
--%>
<%@page import="java.util.Base64"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*" %>
<%! String driver = "oracle.jdbc.driver.OracleDriver";%>
<%!String url = "jdbc:oracle:thin:@localhost:1521:xe";%>
<%!String username = "BloodDonor";%>
<%!String password = "123";%>
<%
    try {
        Class.forName(driver);
    } catch (ClassNotFoundException e) {
        e.printStackTrace();
    }
    Connection connection = null;
    Statement statement = null;
    ResultSet resultSet = null;
%>
<html>
    <head>
        <style>
            .navbar {
                overflow: hidden;
                height:80px;
                background-color: #333;
            }
            .navbar a {
                float: left;
                font-size: 30px;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }
            .dropdown {
                float: left;
                overflow: hidden;
            }
            .dropdown .dropbtn {
                font-size: 30px;  
                border: none;
                outline: none;
                color: white;
                padding: 14px 16px;
                background-color: inherit;
                font-family: inherit;
                margin: 0;
            }
            .navbar a:hover, .dropdown:hover .dropbtn {
                background-color: red;
            }
            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                z-index: 1;
            }
            .box{
                width: 700px;
                height: 70px;
                padding: 20px;
                margin: 100px auto;
                margin-top: auto;
                background-position: center;
                box-shadow:0px grey;
                border:10px ;
                background-image:url(image5.jpg);
                background-image:url(image6.jpg);
            }
            .mySlides {
                display:none;
                margin-left:initial;
                margin-right:inherit;
                height:250px;
            }
            .dropdown-content a {
                float: none;
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
                text-align: left;
            }
            .dropdown-content a:hover {
                background-color: #ddd;
            }
            .dropdown:hover .dropdown-content {
                display: block;
            }
            .navbar {
                overflow: hidden;
                height:40px;
                background-color: #333;
            }
            .navbar a {
                float: left;
                font-size: 25px;
                color: white;
                text-align: center; 
                padding: 14px 16px;
                text-decoration: none;
            }
            .navbar a:hover, .dropdown:hover .dropbtn {
                background-color: green;
            }
        </style>
    </head>
    <body style="background-color:  #ff6666">
  <div class="navbar" style="background-color:  #1a0033">
               <a href="index.html">HOME</a>
		 <a href="SearchDonors.jsp">SEARCH DONORS</a>
                 <a href="AboutUs.jsp">ABOUT US</a>
		 <a href="BloodTips.jsp">BLOOD TIPS</a>
                 <a href="RequestBlood.jsp">REQUEST BLOOD</a>
		 <a href="ContactUs.jsp">CONTACT US</a>   
            </div>
<div class="control-label col-sm-8" align="top" >
        <img class="mySlides" src="AB+group.jpg" style="width: 100%">
        <img class="mySlides" src="Quotation.png" style="width:100%">
        <img class="mySlides" src="DonateSavelife.jpg" style="width:100%">
</div>

        <div class="container">
            <div style="text-align:center">
                <h2>SEARCH RESULTS</h2>
                <p>You need a big heart and free mind for blood donation and not money and strength.</p>
            </div>
            <div class="column">
                <div class="control-label col-sm-6" align="top" ><br></br>
                    <table border="1">
                        <tr>
                            <td>
                                <h2>Patient Name</h2>
                            </td>
                            <td>
                                <h2>Blood Group</h2>
                            </td>
                            <td>
                                <h2>Hospital Address</h2>
                            </td>
                            <td>
                                <h2>Contact Name</h2>
                            </td>
                            <td>
                                <h2>Contact Number</h2>
                            </td>
                            <td>
                                <h2>Contact Email</h2>
                            </td>
                            <td>
                                <h2>Request Details</h2>
                            </td>
                        </tr>
                        <%
                            try {
                                connection = DriverManager.getConnection(url, username, password);
                                statement = connection.createStatement();
                                String Address = request.getParameter("hospitaladdress");
                                String Bloodgroup = request.getParameter("BloodGroup");
                                System.out.println("hospitaladdress" + Address);
                                System.out.println("BloodGroup" + Bloodgroup);
                                String sql = "select * from Blood";
                                System.out.println(sql);
                                resultSet = statement.executeQuery(sql);
                                while (resultSet.next()) {
                        %>
                        <tr>
                            <td><%=resultSet.getString("patient_Name")%></td>
                            <td><%=resultSet.getString("Blood_Group")%></td>
                            <td><%=resultSet.getString("hospital_address")%></td>
                            <td><%=resultSet.getString("contact_Name")%></td>
                            <td><%=resultSet.getString("contact_Number")%></td>
                            <td><%=resultSet.getString("contact_Email")%></td>
                            <td><a href="EmailForDonor.jsp?id=<%=resultSet.getString("pateint_id")%>">Request</a></td>
                        </tr>
                        <%
                                }
                                connection.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        %>
                </div>
                </table>

                </body>
                <script>
                    var myIndex = 0;
                    carousel();
                    function carousel() {
                        var i;
                        var x = document.getElementsByClassName("mySlides");
                        for (i = 0; i < x.length; i++) {
                            x[i].style.display = "none";
                        }
                        myIndex++;
                        if (myIndex > x.length) {
                            myIndex = 1
                        }
                        x[myIndex - 1].style.display = "block";
                        setTimeout(carousel, 1000); // Change image every 2 seconds
                    }
                </script>

                </html>