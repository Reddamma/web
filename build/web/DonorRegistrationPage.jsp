<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Form validation with JavaScript</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <style>
            .container {
                padding: 10px;
            }

            #phoneId {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }
            #emailId {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            #passwordId {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            #nameId {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            #ConfirmPasswordId {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            #emailId {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            #donorId {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            #ageId {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            #addressId {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }

            .registerbtn {
                background-color: #4CAF50;
                color: white;
                padding: 12px 20px;
                margin: 5px 0;
                border: none;
                cursor: pointer;
                width: 100%;
                opacity: 0.9;
            }
            .mySlides {
                display:none;
                margin-left:initial;
                margin-right:inherit;
                height:730px;
            }
            form {
                background: url('background4.jpg');
                background-size: 1600px 1000px;
                width: 90%;
                margin-left: 1px;
                margin-top: 1px;
                height: 700px;
                margin: 10px auto;
            }

            form div {
                margin: 1px auto;
            }
            .navbar {
                overflow: hidden;
                height:40px;
                background-color: #333;
            }
            .navbar a {
                float: left;
                font-size: 25px;
                color: white;
                text-align: center; 
                padding: 14px 16px;
                text-decoration: none;
            }
            .navbar a:hover, .dropdown:hover .dropbtn {
                background-color: green;
            }
        </style>
    </head>
    <body style="background-color:  #e699cc">
        <div class="container">
            <div class="navbar" style="background-color:  #1a0033">
                <a href="DonorLogin.jsp">Back</a>        
            </div>
            <div class="row">
                <div class="control-label col-sm-6" align="left" >
                    <img class="mySlides" src="download5.jpg" style="width: 100%">
                    <img class="mySlides" src="DonateBloodImage.jpg" style="width: 100%">
                    <img class="mySlides" src="download2.png" style="width:100%">
                    <img class="mySlides" src="download5.jpg" style="width:100%">


                </div>
                <div class="column">
                    <div class="control-label col-sm-6" align="top" >

                        <form action="reg" method="POST">
                            <h1>
                                <center>Registration Details</center>
                            </h1><br>
                            <div>
                                <label>userName</label> <input type="text" id="nameId"
                                                               placeholder="Name" required name="userName" />
                            </div>
                            <div>
                                <label>Password</label> <input type="password" id="passwordId"
                                                               placeholder=" password" name="password" required />
                            </div>
                            <div>
                                <label>MobileNumber</label>
                                <input type="tel" id="phoneId"
                                       placeholder=" mobileNumber" name="mobilenumber" required />
                            </div>
                            <div>
                                <label>Gender:</label>
                                <input type="radio" name="gender"value="male" checked>Male 
                                <input type="radio" name="gender"value="female">Female
                            </div>
                            <div>
                                <label>Age</label> 
                                <input type="age" id="ageId"
                                       placeholder=" age.." name="age" required />
                            </div>
                            <div>
                                <label>Email</label> <input type="email" id="emailId"
                                                            placeholder=" Email" name="email" required />
                            </div>
                            <div>
                                <label>Address</label> 
                                <input type="address" id="addressId"
                                       placeholder=" address" name="address" required />
                            </div><br>

                            <div>
                                <label>Blood group</label>
                                <select name="BloodGroup">
                                    <option value="A+">A+</option>
                                    <option value="B+">B+</option>
                                    <option value="O+">O+</option>
                                    <option value="O-">O-</option>
                                    <option value="A-">A-</option>
                                    <option value="B-">B-</option>
                                    <option value="AB+">AB+</option>
                                    <option value="AB-">AB-</option>
                                </select>
                            </div><br></br>

                            <div>
                                <input type="submit" class="registerbtn"/>

                            </div><br>
                            <div class="container signin">
                                <a href="DonorLogin.jsp"></a>
                                <a href="DonorRegistrationPage.jsp"></a>
                            </div>

                        </form>
                    </div>

                    </body>
                    <script>
                        var myIndex = 0;
                        carousel();
                        function carousel() {
                            var i;
                            var x = document.getElementsByClassName("mySlides");
                            for (i = 0; i < x.length; i++) {
                                x[i].style.display = "none";
                            }
                            myIndex++;
                            if (myIndex > x.length) {
                                myIndex = 1
                            }
                            x[myIndex - 1].style.display = "block";
                            setTimeout(carousel, 1000); // Change image every 2 seconds
                        }
                    </script>

                    </html>