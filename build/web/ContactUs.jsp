<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>


<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <style>
            body {
                font-family: Arial, Helvetica, sans-serif;

            }
            * {
                box-sizing: border-box;
            }

            /* Style inputs */
            input[type=text], select, textarea {
                width: 100%;
                padding: 12px;
                border: 1px solid #ccc;
                margin-top: 6px;
                margin-bottom: 16px;
                resize: vertical;
            }

            input[type=submit] {
                background-color: #4CAF50;
                color: white;
                padding: 12px 20px;
                border: none;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            /* Style the container/contact section */
            .container {
                border-radius: 5px;
                background-color: #f2f2f2;
                padding: 10px;
            }

            /* Create two columns that float next to eachother */
            .column {
                float: left;
                width: 50%;
                margin-top: 6px;
                padding: 20px;
            }

            /* Clear floats after the columns */
            .row:after {
                content: "";
                display: table;
                clear: both;
            }

            /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
            @media screen and (max-width: 600px) {
                .column, input[type=submit] {
                    width: 50%;
                    margin-top: 0;
                }
            }
            .navbar {
                overflow: hidden;
                height:40px;
                background-color: #333;
            }
            .navbar a {
                float: left;
                font-size: 25px;
                color: white;
                text-align: center; 
                padding: 14px 16px;
                text-decoration: none;
            }
          input[type=submit]:hover {
                background-color: #45a049;
            }
        </style>
    </head>
    <body style="background-color:#ff99cc">

        <div class="container">
            <div class="navbar" style="background-color:  #1a0033">
                <a href="index.html">HOME</a>
		 <a href="SearchDonors.jsp">SEARCH DONORS</a>
                 <a href="AboutUs.jsp">ABOUT US</a>
		 <a href="BloodTips.jsp">BLOOD TIPS</a>
                 <a href="RequestBlood.jsp">REQUEST BLOOD</a>
		 <a href="ContactUs.jsp">CONTACT US</a>          
            </div>
            <div style="text-align:center">
                <h2>Contact Us</h2>
                <p>You need a big heart and free mind for blood donation and not money and strength.</p>
            </div>
            <div class="row">
                <div class="control-label col-sm-6" align="top" >
                    <img class="mySlide" src="Quotation.png" style="width:100%">
                </div>
                <div class="column">
                    <div class="control-label col-sm-6" align="top" >
                        <form action="WelcomeLogin.html" method="post">
                            <label for="fname">First Name</label>
                            <input type="text" id="fname" name="firstname" placeholder="Your name..">
                            <label for="lname">Last Name</label>
                            <input type="text" id="lname" name="lastname" placeholder="Your last name..">
                            <label for="country">City</label>
                            <select id="city" name="city">
                                <option value="Tirupati">Tirupati</option>
                                <option value="Chennai">Chennai</option>
                                <option value="Bangalore">Bangalore</option>
                                <option value="Nellore">Nellore</option>
                                <option value="Chittoor">Chittoor</option>
                                <option value="Vijayavada">Vijayavada</option>
                                <option value="Visakapatnam">Visakapatnam</option>
                                <option value="Hydhrabad">Hydhrabad</option>
                            </select>
                            <label for="subject">Subject</label>
                            <textarea id="subject" name="subject" placeholder="Write something.." style="height:170px"></textarea>
                            <input type="submit" value="Submit">
                            <div><a href="WelcomeLogin.html"></a>
                        </form>
                    </div>
                </div>
            </div>

    </body>
</html>
